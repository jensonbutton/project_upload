<?php
    include 'helper.php';    
    $cid = $_GET['id'];
?>
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container">
        <h1 class="text-center header"
            style="border: 2px solid gray; box-sizing: border-box; background-color:lightblue">Category Section</h1>
           <?php
                   $sql = $db->select('*',"tblcategory",""," WHERE cid='{$cid}'","","") or die(mysqli_error($db->conn));
                   
                   foreach ($sql as $key => $value) {
                    $id = $value['cid'];
                    $catname = $value['name'];
                    $catimage = $value['image'];
                    $added_date = $value['added_date'];
                    $modified_date = $value['modified_date'];
                    $catorder = $value['catorder'];
                    $catstatus = $value['catstatus'];
?>
<form action="updatecategory.php" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label>Category Name</label>
                <input type="hidden" name="cid" value="<?php echo $id?>"/>
                <input type="text" name="update_name" class="form-control" placeholder="Enter Category Name" value="<?php echo $catname?>"/>
            </div>
            <div class="form-group row">
                <div class="col-xs-4">
                    <label>Order</label>
                    <input type="number" name="update_order" class="form-control" placeholder="Enter Order" value="<?php echo $catorder?>"/>
                </div>
                <div class="col-xs-3">
                <label>Status</label>
                <select class="form-control" name="update_status">
                <option <?php if($catstatus == 'active') echo "selected=selected"?> value="active">active</option>
                        <option <?php if($catstatus == 'inactive') echo "selected=selected"?> value="inactive">inactive</option>
                </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                <label>Upload Image</label>
                <input type="file" name="image" class="form-control"  value="<?php echo $catimage?>">
                <img src="images/<?php echo $catimage;?>" width="100px" height="100px">
            </div>
            </div>
           <div class="form-group">
            <a href="c_index.php" class="btn btn-danger"> Cancel </a>
            <input type="submit" name="edit" value="Update" class="btn btn-primary">
           </div>
        </form>
        <?php }
    
?>
    </div>
</body>

</html>