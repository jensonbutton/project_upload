<?php
class Database
{
    public $dbHost;
    public $dbUser;
    public $dbPass;
    public $dbName;
    public $conn;
    

    public function __construct()
    {
        $this->dbHost = "localhost";
        $this->dbUser = "root";
        $this->dbPass = "";
        $this->dbName = "project";
        $this->conn = "";
      
        $this->conn = mysqli_connect($this->dbHost, $this->dbUser, $this->dbPass, $this->dbName);
        if (!$this->conn) {
            die("connection fail");
        }
    }

    
    public function select($sel, $table, $JOINS, $where, $GROUPBY,$order)
    {
        //  $where="";
        //  if($values!=""){
        //  foreach($values as $key =>$value){  $where .=$key."='".$value."'";}
        // print_r($where);
        // exit();
        //  }
        $query = "SELECT $sel from $table" . $JOINS . $where . $GROUPBY . $order;
        // echo $query;
        // exit();
        $result = mysqli_query($this->conn, $query);
        $return = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $return[] = $row;
            // echo "<pre/>";
            //  print_r($return);exit;
            //print_r($return);
            //exit();
        }
        return $return;
        //echo $where;
    }


    public function insert($table, $field)
    {
        $key = array_keys($field);
        $val = array_values($field);
        $return = [];
        $query = "INSERT into  $table(". implode(',', $key) . ")" . "Values('" . implode("','", $val) . "')";
        //   echo $query;
        //    exit();
         $result = mysqli_query($this->conn, $query);
        // //mysqli_insert_id($this->conn);
        // // echo $result;
        // return $result;
        if($result == 1)
        {
            $this->id = mysqli_insert_id($this->conn);
            return true;
        }else{
            return false;
        }

    }


    public function update($table, $values, $where)
    {
        $set = [];
        foreach ($values as $key => $value) {
            $set[] = $key . "='" . $value . "'";
         }
        $query = "UPDATE $table SET " . implode(',', $set) . " " . "WHERE" . " " . $where;
        // echo $query;
        // exit();
        $result = mysqli_query($this->conn, $query);
        return $result;
    }



    public function delete($select,$table, $values)
    {
        $where = "";
        $and = "AND";
        if ($values != "") {
            foreach ($values as $key => $value) {
                $where .= $key . "='" . $value . "'";
                        //p.pid='139'i.product_id='139'
            }
            // print_r($where);
            // exit();
        }
        $query = "DELETE $select FROM $table" . " " . "WHERE" . " " . $where;
        // echo $query;
        // exit();
        $result = mysqli_query($this->conn, $query);
        return $result;
    }


    // public function getInsertId()
    // {
    //     $_result = mysqli_insert_id($this->conn);
    //     $this->_insertId = $_result;
    //     return $_result;
    // }

    //  public function lastid($tabel,$id)
    //  {
    //    $select = "SELECT * FROM $tabel ORDER BY $id desc limit 1" ;
    //     // echo $select;
    //     // exit();
    //    $result1 = mysqli_query($this->conn,$select);
    //    $row = mysqli_fetch_array($result1);
    //    $last = $row[$id];
    //    return $last;
    //  }

    function image_upload($image, $tmp_name, $path)
    {
        $new_name = ltrim(time() . "-" . rand(1000, 9999) . "-" . $image);
        $targetFilePath = $path . $new_name;
        $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
        $allowed = array("jpg", "jpeg", "png");
        if (in_array($fileType, $allowed)) {
            move_uploaded_file($tmp_name, $targetFilePath);
            return $new_name;
        } else {
            return false;
      }
    }

    function mul_image_upload($filename,$img_name,$path)
    {
        foreach ($filename as $key => $value) {
            $imagename = $img_name[$key];
            $imagetemp = $filename[$key];

            $image_upload = move_uploaded_file($imagetemp, $path.$imagename);
            return $imagename;
        }


    }
}

$db = new Database();
