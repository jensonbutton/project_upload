<?php
    include 'helper.php';
    
    if(isset($_GET['pid']) && isset($_GET['img_id']))
    {

        $delid = $_GET['img_id'];
        $pid = $_GET['pid'];
    
            // $select = "SELECT * FROM tblproduct_image WHERE id = '$delid'";
            // $result = mysqli_query($conn,$select) OR die(mysqli_error($conn));
            $where3 = " WHERE id = '$delid'";
            $sql3 = $db->select("*","tblproduct_image","","$where3","","");

                foreach($sql3 as $key3 => $value3){
                    $image = $value3['product_image'];
                    $file = 'productimage/'.$image;
                    unlink($file);
                }
                // $delete = "DELETE FROM tblproduct_image WHERE id='$delid'";
                // $result1 = mysqli_query($conn,$delete) OR die(mysqli_error($conn));
                $val = array('id' => $delid); 
                $del = $db->delete("","tblproduct_image",$val);
                header('Location:editproduct.php?pid='.$pid.'');
            }
    elseif (isset($_POST['editproduct'])) {

        $edit_id = $_POST['pid'];
        $edit_name = $_POST['update_name'];
        $edit_cat = $_POST['update_cat'];
        $edit_price = $_POST['price'];
        $edit_sellprice = $_POST['sellprice'];
        $edit_quantity = $_POST['quantity'];
        $edit_order = $_POST['order'];
        $edit_status = $_POST['status'];
        $edit_date = date("Y-m-d");
        $filename = $_FILES['update_image']['tmp_name'];
        $img_name = $_FILES['update_image']['name'];
        $path = 'productimage/';

        if($img_name != '')
        {
            $val1 = array('p_name' => $edit_name,'c_name' => $edit_cat,'price' => $edit_price,'sale_price' => $edit_sellprice,'quantity' => $edit_quantity,'modified_date' => $edit_date,'p_order' => $edit_order,'p_status' => $edit_status);
            $where4 = "pid = '{$edit_id}'";
            $update1 = $db->update("tblproduct",$val1,"$where4");
            
            

            if($update1 == 1)
            {
                $lastid = mysqli_insert_id($db->conn);
            }
            foreach ($filename as $key => $value) {
                $imagename = $img_name[$key];
                $imagetemp = $filename[$key];
    
                if(move_uploaded_file($imagetemp, $path.$imagename))
                {
                    $Status = "inactive";
                    $field2 = ['product_id' => $edit_id,'product_image' => $imagename,'status' => $Status];
                    $insert_image = $db->insert('tblproduct_image',$field2) or die(mysqli_error($db->conn));
                }
            }



        }else{

            $val2 = array('p_name' => $edit_name,'c_name' => $edit_cat,'price' => $edit_price,'sale_price' => $edit_sellprice,'quantity' => $edit_quantity,'modified_date' => $edit_date,'p_order' => $edit_order,'p_status' => $edit_status);
            $where5 = "pid = '{$edit_id}'";
            $update2 = $db->update("tblproduct",$val2,"$where5");

        }
        header('Location:p_index.php');

    }
    


  ?>
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<script>
    
function myfun(){
		return confirm("Are you sure?");
	}
</script>
<body>
    <div class="container">
        <h1 class="text-center header"
            style="border: 2px solid gray; box-sizing: border-box; background-color:lightblue">Update Product Section</h1>
            <?php
            
            $pid = $_GET['pid'];
                $sql = $db->select('*',"tblproduct",""," WHERE pid='{$pid}'","","") or die(mysqli_error($db->conn));
                foreach ($sql as $key => $value) {
            ?>

        <form action="" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label>Product Name</label>
                <input type="hidden" name="pid" value="<?php echo $value['pid']?>"/>
                <input type="text" name="update_name" class="form-control" value="<?php echo $value['p_name']?>">
            </div>
            <div class="form-group row">
                <div class="col-xs-6">
                    <label>Category</label>
                    <?php
                      $sql1 = $db->select('*',"tblcategory","","","","") or die(mysqli_error($db->conn));
                            echo '<select name="update_cat" class="form-control">';
                            foreach ($sql1 as $key1 => $value1) {
                            
                                if($value['c_name'] == $value1['cid'])
                                {
                                    $select = "selected";
                                }
                                else
                                {
                                    $select = "";
                                }
                                echo "<option {$select} value='{$value1['cid']}'>{$value1['name']}</option>";
                            }
                            echo '</select>';    
                        
                    ?>
                </div>
                <div class="col-xs-4">
                    <label>Product Code</label>
                    <input type="text" readonly name="productcode" class="form-control"
                    value="<?php echo $value['Product_code']?>">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    <label>Price</label>
                    <input type="number" name="price" class="form-control" value="<?php echo $value['price']?>">
                </div>
                <div class="col-lg-3">
                    <label>Sell Price</label>
                    <input type="number" name="sellprice" class="form-control" value="<?php echo $value['sale_price']?>">
                </div>
                <div class="col-lg-3">
                    <label>Quantity</label>
                    <input type="number" name="quantity" class="form-control" value="<?php echo $value['quantity']?>">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    <label>Order</label>
                    <input type="number" name="order" class="form-control" value="<?php echo $value['p_order']?>">
                </div>
                <div class="col-lg-4">
                    <label>Status</label>
                    <select class="form-control" name="status">
                    <option <?php if($value['p_status'] == 'active') echo "selected=selected"?> value="active">active</option>
                        <option <?php if($value['p_status'] == 'inactive') echo "selected=selected"?> value="inactive">inactive</option>
                    </select>
                </div>
                <div class="col-lg-3">
                    <label>Upload Main Image</label>
                    <?php

                        $sql2 = $db->select('*',"tblproduct_image",""," WHERE product_id='{$pid}'","","") or die(mysqli_error($db->conn));    
                        foreach ($sql2 as $key2 => $value2) {

                                if($value2['status'] == 'active')
                                {
                                    $image[] = $value2['product_image'];
                                } 
                                elseif($value2['status'] == 'inactive')
                                {
                                    $in_image[] = $value2['product_image'];
                                    $new_id[] = $value2['id'];
                                }
                            } 
                    ?>
                    <input type="file" name="update_image[]" class="form-control" multiple value="<?php echo $value2['product_image'];?>" >
                    
                    <?php for ($i = 0; $i < count($image); $i++) { ?>
                        <img src="productimage/<?php echo $image[$i];?>" width="100px" height="100px"><br><br>
                        <?php } ?>
                        <?php for ($i = 0; $i < count($in_image); $i++) { ?>
                            <img src="productimage/<?php echo $in_image[$i];?>" width="100px" height="100px">
                            <a href='editproduct.php?pid=<?php echo $pid?>&img_id=<?php echo $new_id[$i];?>' class='btn btn-danger' onclick="return myfun()">Delete</a>
                         <a href='image_active.php?pid=<?php echo $pid?>&img_status=<?php echo $new_id[$i];?>' class='btn btn-primary'>Active</a>
                <?php } ?>
                                
            </div>                   
            </div>
            <input type="submit" name="editproduct" value="Update" class="btn btn-primary">
            <a href="p_index.php" class="btn btn-success">Cancel</a>
        </form>
        <?php     
            }
             
        ?> 
    </div>
</body>

</html>