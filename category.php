<?php
include 'welcome.php';
    // session_start();

    if($_SESSION['username']==true){
        
    }else{
        header('location:login.php');
    }
?>
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://parsleyjs.org/dist/parsley.js"></script>
    <style type="text/css">.req {margin:2px;color:#dc3545;}.serif {font-family: "Times New Roman", Times, serif;}li.parsley-required {
    color: red;
	}</style>
</head>

<body>
    <div class="container">
        <h1 class="text-center header"
            style="border: 2px solid gray; box-sizing: border-box; background-color:lightblue">Category Section</h1>
        <form action="savecategory.php" data-parsley-validate="" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label>Category Name</label>
                <input type="text" name="c_name" class="form-control" placeholder="Enter Category Name" required>
            </div>
            <div class="form-group row">
                <div class="col-xs-4">
                    <label>Order</label>
                    <input type="text" name="order" class="form-control" placeholder="Enter Order" required>
                </div>
                <div class="col-xs-3">
                    <label>Status</label>
                    <select class="form-control" name="status">
                        <option value="">-- Select Status --</option>
                        <option value="active">Active</option>
                        <option value="inactive">InActive</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    <label>Upload Image</label>
                    <input type="file" name="c_image" class="form-control" required>
                </div>
            </div>
            <input type="submit" name="submit" value="Submit" class="btn btn-primary">
            <a href="c_index.php" class="btn btn-success">Category List</a>
        </form>
    </div>
</body>


</html>