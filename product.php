<?php
    include 'helper.php';
    include 'welcome.php';
    // session_start();

    if($_SESSION['username']==true){
        
    }else{
        header('location:login.php');
    }

   if(isset($_POST['submit']))
  {


        $Name = $_POST['p_name'];
        $Category = $_POST['category'];
        $pcode = $_POST['productcode'];
        $Price = $_POST['price'];
        $Sellprice = $_POST['sellprice'];
        $Quantity = $_POST['quantity'];
        $Order = $_POST['order'];
        $pro_Status = $_POST['status'];
        $filename = $_FILES['p_image']['tmp_name'];
        $img_name = $_FILES['p_image']['name'];
        $path = 'productimage/';
        
            $field = ['p_name' => $Name,'c_name' => $Category,'Product_code' => $pcode,'price' => $Price,'sale_price' => $Sellprice,'quantity' => $Quantity,'p_order' => $Order,'p_status' => $pro_Status];
            $insert = $db->insert('tblproduct',$field) or die(mysqli_error($db->conn));        
                if($insert == 1)
                {
                    $lastid = mysqli_insert_id($db->conn);
                }
                foreach ($filename as $key => $value) {
                    $imagename = $img_name[$key];
                    $imagetemp = $filename[$key];

                    if(move_uploaded_file($imagetemp, $path.$imagename))
                    {
                        if($key == 0)
                        {
                            $Status = 'active';
                        }
                        else
                        {
                            $Status = 'inactive';
                        }
                        $field1 = ['product_id' => $lastid,'product_image' => $imagename,'status' => $Status];
                        $insert_image = $db->insert('tblproduct_image',$field1) or die(mysqli_error($db->conn));
                    }
                }


                header("Location: http://localhost/Week-5/p_index.php");

  }


?>
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container">
        <h1 class="text-center header"
            style="border: 2px solid gray; box-sizing: border-box; background-color:lightblue">Product Section</h1>
        <form action="" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label>Product Name</label>
                <input type="text" name="p_name" class="form-control" placeholder="Enter Product Name" required>
            </div>
            <div class="form-group row">
                <div class="col-xs-6">
                    <label>Category</label>
                    <select class="form-control" name="category">
                        <option value="">--Select Category--</option>
                        <?php
                                    // $select = "SELECT * from tblcategory";
                                    // $result = mysqli_query($conn,$select);
                                    // while($row = mysqli_fetch_array($result))
                                    // {
                                    //         $c_id = $row['cid'];
                                    //         $Name = $row['name'];
                                    //         echo "<option value='$c_id'>$Name</option>";
                                    // }
                                        $where = " WHERE catstatus = 'active'";
                                    $sql = $db->select("*","tblcategory","","$where","","");
                                    foreach ($sql as $key => $value) {
                                        $c_id = $value['cid'];
                                        $name = $value['name'];
                                        echo "<option value='$c_id'>$name</option>";
                                    }
                        ?>
                    </select>
                </div>
                <div class="col-xs-4">
                    <label>Product Code</label>
                    <input type="text" readonly name="productcode" class="form-control"
                        value="<?php echo rand(1000,99999); ?>">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    <label>Price</label>
                    <input type="number" name="price" class="form-control" placeholder="Enter Price" required>
                </div>
                <div class="col-lg-3">
                    <label>Sell Price</label>
                    <input type="number" name="sellprice" class="form-control" placeholder="Enter Sell Price" required>
                </div>
                <div class="col-lg-3">
                    <label>Quantity</label>
                    <input type="number" name="quantity" class="form-control" placeholder="Enter Quantity" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    <label>Order</label>
                    <input type="number" name="order" class="form-control" placeholder="Enter Order" required>
                </div>
                <div class="col-lg-4">
                    <label>Status</label>
                    <select class="form-control" name="status">
                        <option value="active">Active</option>
                        <option value="inactive">InActive</option>
                    </select>
                </div>
                <div class="col-lg-3">
                    <label>Upload Image</label>
                    <input type="file" name="p_image[]" class="form-control" multiple required>
                </div>
            </div>
            <input type="submit" name="submit" value="Submit" class="btn btn-primary">
            <a href="p_index.php" class="btn btn-success">Product List</a>
        </form>
    </div>
</body>

</html>