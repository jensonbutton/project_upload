<?php
    include 'helper.php';
    include 'welcome.php';    
// session_start();

if($_SESSION['username']==true){
	
}else{
	header('location:login.php');
}
?>

<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <nav class="navbar">
        <nav>
            <form action="c_index.php" method="POST" class="navbar-form navbar-left">
                <input type="text" class="form-control" name="search" placeholder="Search name">
                <input type="submit" name="btn_search" class="btn btn-primary" value="Find">
                <a href="category.php" class="btn btn-success btn-right">Add Category</a>
            </form>
        </nav>
    </nav>

    <div class="container">
        <?php
            if(isset($_POST['btn_search']))
            {
                $search = $_POST['search'];
                if($search = "")
                {
                    $select = '*,tblcategory.added_date,tblcategory.modified_date,count(c_name) AS p_no';
                    $join = " tblcategory LEFT JOIN tblproduct on tblcategory.cid = tblproduct.c_name";
                    $sql = $db->select($select,"","$join",""," GROUP BY cid","") or die(mysqli_error($db->conn));
                }else{
                    $where = " WHERE name LIKE '%$search%'";
                    $select = '*,tblcategory.added_date,tblcategory.modified_date,count(c_name) AS p_no';
                    
                    $join = " tblcategory LEFT JOIN tblproduct on tblcategory.cid = tblproduct.c_name";
                    $sql = $db->select($select,"","$join","$where"," GROUP BY cid","") or die(mysqli_error($db->conn));
                }
                

            }else{
                $select = '*,tblcategory.added_date,tblcategory.modified_date,count(c_name) AS p_no';
                $join = " tblcategory LEFT JOIN tblproduct on tblcategory.cid = tblproduct.c_name";
                $sql = $db->select($select,"","$join",""," GROUP BY cid","") or die(mysqli_error($db->conn));
                
            }
            //     $search = $_POST['search'];
            //     $select = "SELECT *,tblcategory.order AS c_order,tblcategory.status as c_status , count(pid) AS P_no FROM tblcategory JOIN tblproduct ON tblcategory.cid = tblproduct.c_name WHERE name LIKE '%$search%'  GROUP BY c_name";
            // }
            // else
            // {
                
                //             //$select = "SELECT *,tblcategory.order AS c_order,tblcategory.status as c_status , count(pid) AS P_no FROM tblcategory JOIN tblproduct WHERE tblcategory.cid = tblproduct.c_name GROUP BY c_name";
                // }
                //         $result = mysqli_query($conn,$select) or die(mysqli_error($conn));
                //         if(mysqli_num_rows($result) > 0){
                    // $join = " tblcategory JOIN tblproduct WHERE tblcategory.cid = tblproduct.c_name";
                    // ,count(pid) AS p_no
                    //$groupby = ' GROUP BY c_name';
                    // ,count(pid) AS p_no
                ?>
                                                                        
        <table cellpadding="7px" style="border: 2px solid gray;" class="table table-solid">
            <thead>
                <th hidden>ID</th>
                <th>Name</th>
                <th>Category image</th>
                <th>Added Date</th>
                <th>Modify Date</th>
                <th>order</th>
                <th>Status</th>
                <th>Number Of Product</th>
                <th>Action</th>
            </thead>
            <tbody>
                <?php    
                   // while($row = mysqli_fetch_assoc($result))
                   foreach ($sql as $key => $value) {
                    $id = $value['cid'];
                    $catname = $value['name'];
                    $catimage = $value['image'];
                    $added_date = $value['added_date'];
                    $modified_date = $value['modified_date'];
                    $catorder = $value['catorder'];
                    $catstatus = $value['catstatus'];
                     $pno = $value['p_no'];
                   
                    ?>
                <tr>
                    <td hidden><?php echo $id;?></td>
                    <td><?php echo $catname;?></td>
                    <td><img src="images/<?php echo $catimage;?>" width="100px" height="100px"></td>
                    <td><?php echo $added_date;?></td>
                    <td><?php echo $modified_date;?></td>
                    <td><?php echo $catorder;?></td>
                    <td><?php echo $catstatus;?></td>
                    <td><?php echo $pno;?></td>
                    <td>
                        <a href='editcategory.php?id=<?php echo $id;?>' class="btn btn-primary">Edit</a>
                        <a href='c_delete.php?id=<?php echo $id;?>' class="btn btn-danger">Delete</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
       </div>
</body>

</html>